package trend.analyzer.chart.calculator;

import java.time.format.DateTimeFormatter;
import java.util.Comparator;

import javafx.scene.chart.XYChart;
import trend.analyzer.alphavantage.stock.domain.ExchangeRate;
import trend.analyzer.alphavantage.stock.domain.StockTimeSeries;
import trend.analyzer.chart.CalculatorStrategy;

public class HighPriceCalculator implements CalculatorStrategy {

    @Override
    public XYChart.Series calculate(StockTimeSeries stockTimeSeries) {
        XYChart.Series series = new XYChart.Series();
        series.setName("High");
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        stockTimeSeries.getSeries().stream()
            .sorted(Comparator.comparing(ExchangeRate::getTime))
            .forEachOrdered(item -> series.getData().add(new XYChart.Data(item.getTime().format(dateTimeFormatter), item.getPrice().getHigh())));
        return series;
    }

}
