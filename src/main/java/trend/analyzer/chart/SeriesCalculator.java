package trend.analyzer.chart;

import javafx.scene.chart.XYChart;
import trend.analyzer.alphavantage.stock.domain.StockTimeSeries;

public class SeriesCalculator implements CalculatorStrategy {

    private CalculatorStrategy strategy;

    public SeriesCalculator(CalculatorStrategy strategy) {
        this.strategy = strategy;
    }

    @Override
    public XYChart.Series calculate(StockTimeSeries stockTimeSeries) {
        return strategy.calculate(stockTimeSeries);
    }

}
