package trend.analyzer.chart;

import javafx.scene.chart.XYChart;
import trend.analyzer.alphavantage.stock.domain.StockTimeSeries;

public interface CalculatorStrategy {

    XYChart.Series calculate(StockTimeSeries stockTimeSeries);

}
