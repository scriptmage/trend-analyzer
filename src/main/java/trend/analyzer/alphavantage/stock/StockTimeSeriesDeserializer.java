package trend.analyzer.alphavantage.stock;

import static trend.analyzer.alphavantage.stock.domain.ExchangeRateFieldOrders.CLOSE;
import static trend.analyzer.alphavantage.stock.domain.ExchangeRateFieldOrders.HIGH;
import static trend.analyzer.alphavantage.stock.domain.ExchangeRateFieldOrders.LOW;
import static trend.analyzer.alphavantage.stock.domain.ExchangeRateFieldOrders.OPEN;
import static trend.analyzer.alphavantage.stock.domain.ExchangeRateFieldOrders.VOLUME;
import static trend.analyzer.alphavantage.stock.domain.MetaFieldOrders.INFORMATION;
import static trend.analyzer.alphavantage.stock.domain.MetaFieldOrders.LAST_REFRESHED;
import static trend.analyzer.alphavantage.stock.domain.MetaFieldOrders.SIZE;
import static trend.analyzer.alphavantage.stock.domain.MetaFieldOrders.SYMBOL;
import static trend.analyzer.alphavantage.stock.domain.MetaFieldOrders.TIME_ZONE;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Map;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.ObjectCodec;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import trend.analyzer.alphavantage.stock.domain.ExchangeRate;
import trend.analyzer.alphavantage.stock.domain.ExchangeRateFieldOrders;
import trend.analyzer.alphavantage.stock.domain.MetaData;
import trend.analyzer.alphavantage.stock.domain.MetaFieldOrders;
import trend.analyzer.alphavantage.stock.domain.Price;
import trend.analyzer.alphavantage.stock.domain.StockTimeSeries;
import trend.analyzer.alphavantage.stock.domain.TimePeriods;

public class StockTimeSeriesDeserializer extends JsonDeserializer<StockTimeSeries> {

    @Override
    public StockTimeSeries deserialize(JsonParser parser, DeserializationContext context) throws IOException {
        ObjectCodec oc = parser.getCodec();
        JsonNode node = oc.readTree(parser);

        StockTimeSeries.StockTimeSeriesBuilder stockTimeSeries = StockTimeSeries.builder();
        node.fields().forEachRemaining(entry -> {
            if("Meta Data".equals(entry.getKey())) {
                stockTimeSeries.meta(getMetaData(entry.getValue()));
            }
            if(entry.getKey().contains("Time Series")) {
                stockTimeSeries.timePeriod(TimePeriods.fromValue(entry.getKey()));
                parseTimeSeries(stockTimeSeries.build(), entry);
            }
        });

        return stockTimeSeries.build();
    }

    private void parseTimeSeries(StockTimeSeries stockTimeSeries, Map.Entry<String, JsonNode> entry) {
        entry.getValue().fields().forEachRemaining(price -> {
            ExchangeRate.ExchangeRateBuilder exchangeRate = ExchangeRate.builder();

            exchangeRate.time(getExchangeRateTime(price.getKey(), stockTimeSeries.getTimePeriod()));

            Price.PriceBuilder priceBuilder = Price.builder();
            price.getValue().fields().forEachRemaining(element -> {
                if (detectPriceType(element, VOLUME)) {
                    exchangeRate.volume(element.getValue().asInt());
                }
                if (detectPriceType(element, OPEN)) {
                    priceBuilder.open(element.getValue().asDouble());
                }
                if (detectPriceType(element, CLOSE)) {
                    priceBuilder.close(element.getValue().asDouble());
                }
                if (detectPriceType(element, HIGH)) {
                    priceBuilder.high(element.getValue().asDouble());
                }
                if (detectPriceType(element, LOW)) {
                    priceBuilder.low(element.getValue().asDouble());
                }
            });
            exchangeRate.price(priceBuilder.build());
            stockTimeSeries.addExchangeRate(exchangeRate.build());
        });
    }

    private boolean detectPriceType(Map.Entry<String, JsonNode> element, ExchangeRateFieldOrders volume) {
        return element.getKey().equals(volume.getOrder());
    }

    private LocalDateTime getExchangeRateTime(String time, TimePeriods periods) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        String currentTime = time;
        if(periods.equals(TimePeriods.DAILY)) {
            currentTime += " 00:00:00";
        }
        return LocalDateTime.parse(currentTime, formatter);
    }

    private MetaData getMetaData(JsonNode node) {
        MetaData.MetaDataBuilder metaData = MetaData.builder();
        node.fields().forEachRemaining(entry -> {
            if (detectMetaDataType(entry, INFORMATION)) {
                metaData.information(entry.getValue().asText());
            }
            if (detectMetaDataType(entry, SYMBOL)) {
                metaData.symbol(entry.getValue().asText());
            }
            if (detectMetaDataType(entry, LAST_REFRESHED)) {
                metaData.refreshed(entry.getValue().asText());
            }
            if (detectMetaDataType(entry, SIZE)) {
                metaData.size(entry.getValue().asText());
            }
            if (detectMetaDataType(entry, TIME_ZONE)) {
                metaData.timeZone(entry.getValue().asText());
            }
        });
        return metaData.build();
    }

    private boolean detectMetaDataType(Map.Entry<String, JsonNode> entry, MetaFieldOrders order) {
        return entry.getKey().equals(order.getOrder());
    }

}
