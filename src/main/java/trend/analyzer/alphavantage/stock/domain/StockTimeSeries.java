package trend.analyzer.alphavantage.stock.domain;

import java.util.LinkedList;
import java.util.List;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import trend.analyzer.alphavantage.stock.StockTimeSeriesDeserializer;

@Getter
@Builder
@EqualsAndHashCode
@AllArgsConstructor
@JsonDeserialize(using = StockTimeSeriesDeserializer.class)
public class StockTimeSeries {

    private final MetaData meta;
    private final TimePeriods timePeriod;
    private final List<ExchangeRate> series = new LinkedList<>();

    public void addExchangeRate(final ExchangeRate rate) {
        series.add(rate);
    }

}
