package trend.analyzer.alphavantage.stock.domain;

public enum TimePeriods {
    INTRADAY("Time Series (5min)"),
    DAILY("Time Series (Daily)"),
    WEEKLY("Weekly Time Series"),
    MONTHLY("Monthly Time Series");

    /**
     * Time period of series in minutes
     */
    private String period;

    TimePeriods(String period) {
        this.period = period;
    }

    public static TimePeriods fromValue(String value) {
        for (TimePeriods timePeriods : values()) {
            if (String.valueOf(timePeriods.period).equals(value)) {
                return timePeriods;
            }
        }
        return null;
    }
}
