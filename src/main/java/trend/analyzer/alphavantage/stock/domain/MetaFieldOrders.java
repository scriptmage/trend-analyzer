package trend.analyzer.alphavantage.stock.domain;

public enum MetaFieldOrders {
    INFORMATION("1. Information"),
    SYMBOL("2. Symbol"),
    LAST_REFRESHED("3. Last Refreshed"),
    SIZE("4. Output Size"),
    TIME_ZONE("5. Time Zone");

    private String order;

    MetaFieldOrders(String order) {
        this.order = order;
    }

    public String getOrder() {
        return order;
    }

    public static MetaFieldOrders fromValue(String value) {
        for (MetaFieldOrders metaFieldOrders : values()) {
            if (String.valueOf(metaFieldOrders.order).equals(value)) {
                return metaFieldOrders;
            }
        }
        return null;
    }
}
