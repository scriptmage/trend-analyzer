package trend.analyzer.alphavantage.stock.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;

@Getter
@Builder
@EqualsAndHashCode
@AllArgsConstructor
public class MetaData {

    private final String information;
    private final String symbol;
    private final String refreshed;
    private final String size;
    private final String timeZone;

}
