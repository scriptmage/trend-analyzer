package trend.analyzer.alphavantage.stock.domain;

import java.time.LocalDateTime;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;

@Getter
@Builder
@EqualsAndHashCode
@AllArgsConstructor
public class ExchangeRate {

    private final LocalDateTime time;
    private final Integer volume;
    private final Price price;

}
