package trend.analyzer.alphavantage.stock.domain;

public enum ExchangeRateFieldOrders {
    OPEN("1. open"),
    HIGH("2. high"),
    LOW("3. low"),
    CLOSE("4. close"),
    VOLUME("5. volume");

    private String order;

    ExchangeRateFieldOrders(String order) {
        this.order = order;
    }

    public String getOrder() {
        return order;
    }

    public static ExchangeRateFieldOrders fromValue(String value) {
        for (ExchangeRateFieldOrders metaFieldOrders : values()) {
            if (String.valueOf(metaFieldOrders.order).equals(value)) {
                return metaFieldOrders;
            }
        }
        return null;
    }

}
