package trend.analyzer.alphavantage.stock.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;

@Getter
@Builder
@EqualsAndHashCode
@AllArgsConstructor
public class Price {

    private final Double open;
    private final Double high;
    private final Double low;
    private final Double close;

}
