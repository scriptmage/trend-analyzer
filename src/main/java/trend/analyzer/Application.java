package trend.analyzer;

import static trend.analyzer.chart.ChartFactory.createLineChart;

import java.io.IOException;

import com.fasterxml.jackson.databind.ObjectMapper;
import javafx.event.ActionEvent;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.chart.LineChart;
import javafx.scene.control.ComboBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import trend.analyzer.alphavantage.stock.domain.StockTimeSeries;
import trend.analyzer.chart.SeriesCalculator;
import trend.analyzer.chart.calculator.AveragePriceCalculator;
import trend.analyzer.chart.calculator.HighPriceCalculator;
import trend.analyzer.loader.Loader;

public class Application extends javafx.application.Application {

    private static final String INPUT_DATA_FILENAME_TEMPLATE = "data-%s.json";

    private StockTimeSeries stockTimeSeries;
    private ComboBox<String> stockList;

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage window) {
        stockList = new ComboBox<>();
        stockList.getItems().addAll("MSFT", "GOOGL");
        stockList.setPromptText(stockList.getItems().stream().findFirst().get());
        stockList.getSelectionModel().selectFirst();
        stockList.setOnAction(event -> {
            String selectedItem = stockList.getSelectionModel().getSelectedItem();
            stockTimeSeries = getStockTimeSeries(selectedItem);
            refresh(window);
        });
        stockList.fireEvent(new ActionEvent());
    }

    private void refresh(Stage window) {
        SeriesCalculator averageSeries = new SeriesCalculator(new AveragePriceCalculator());
        SeriesCalculator highSeries = new SeriesCalculator(new HighPriceCalculator());

        String title = stockTimeSeries.getMeta().getSymbol() + " " + stockTimeSeries.getMeta().getInformation();
        LineChart lineChart = createLineChart(title, averageSeries.calculate(stockTimeSeries), highSeries.calculate(stockTimeSeries));

        VBox layout = new VBox();
        layout.getChildren().addAll(lineChart, stockList);
        layout.setPadding(new Insets(20, 20, 20, 20));

        Scene scene = new Scene(layout, 1024, 768);

        window.setTitle("Line Chart Sample");
        window.setScene(scene);
        window.show();
    }

    private StockTimeSeries getStockTimeSeries(String symbol) {
        Loader loader = new Loader(new ObjectMapper());
        try {
            return loader.load(String.format(INPUT_DATA_FILENAME_TEMPLATE, symbol));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

}
