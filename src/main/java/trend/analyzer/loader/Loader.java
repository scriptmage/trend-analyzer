package trend.analyzer.loader;

import static java.util.Objects.isNull;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.stream.Collectors;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AllArgsConstructor;
import trend.analyzer.alphavantage.stock.domain.StockTimeSeries;

@AllArgsConstructor
public class Loader {

    final ObjectMapper mapper;

    public StockTimeSeries load(String fileName) throws IOException {
        InputStream inputStream = getClass().getClassLoader().getResourceAsStream(fileName);
        if (isNull(inputStream)) {
            throw new FileNotFoundException("The input file does not exist: " + fileName);
        }
        BufferedReader resReader = new BufferedReader(new InputStreamReader(inputStream));
        String content = resReader.lines().collect(Collectors.joining());
        return mapper.readValue(content, StockTimeSeries.class);
    }

}
