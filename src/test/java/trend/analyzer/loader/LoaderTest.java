package trend.analyzer.loader;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;

import java.io.FileNotFoundException;
import java.io.IOException;

import org.junit.Before;
import org.junit.Test;

import com.fasterxml.jackson.databind.ObjectMapper;
import trend.analyzer.alphavantage.stock.domain.StockTimeSeries;

public class LoaderTest {

    private static final String SAMPLE_JSON_FILE = "stocktimeseries_test_data.json";
    private static final String NOT_EXISTS_JSON = "not_exists.json";

    private Loader underTest;

    @Before
    public void setUp() {
        underTest = new Loader(new ObjectMapper());
    }

    @Test
    public void loadWhenFileDoesNotExistShouldThrowFileNotFoundException() {
        Throwable thrown = catchThrowable(() -> underTest.load(NOT_EXISTS_JSON));

        assertThat(thrown)
            .isInstanceOf(FileNotFoundException.class)
            .hasMessageContaining("The input file does not exist: " + NOT_EXISTS_JSON);
    }

    @Test
    public void loadShouldCreateTimeSeriesFromInputFile() throws IOException {
        StockTimeSeries actual = underTest.load(SAMPLE_JSON_FILE);

        assertThat(actual)
            .isInstanceOf(StockTimeSeries.class);
    }

}
