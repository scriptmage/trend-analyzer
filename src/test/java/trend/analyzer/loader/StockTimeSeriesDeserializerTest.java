package trend.analyzer.loader;

import static java.util.Arrays.asList;
import static org.assertj.core.api.Assertions.assertThat;

import java.io.IOException;
import java.io.InputStream;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.ObjectMapper;
import trend.analyzer.alphavantage.stock.StockTimeSeriesDeserializer;
import trend.analyzer.alphavantage.stock.domain.ExchangeRate;
import trend.analyzer.alphavantage.stock.domain.Price;
import trend.analyzer.alphavantage.stock.domain.StockTimeSeries;
import trend.analyzer.alphavantage.stock.domain.TimePeriods;

public class StockTimeSeriesDeserializerTest {

    private StockTimeSeriesDeserializer underTest;

    private JsonParser parser;
    private DeserializationContext context;

    @Before
    public void setUp() throws IOException {
        underTest = new StockTimeSeriesDeserializer();

        ObjectMapper objectMapper = new ObjectMapper();
        InputStream inputStream = getClass().getClassLoader().getResourceAsStream("stocktimeseries_test_data.json");
        parser = objectMapper.getFactory().createParser(inputStream);
        context = objectMapper.getDeserializationContext();
    }

    @Test
    public void deserializeShouldSetTheProperMetadata() throws IOException {
        StockTimeSeries actual = underTest.deserialize(parser, context);

        assertThat(actual.getMeta().getInformation()).isEqualTo("Information");
        assertThat(actual.getMeta().getSymbol()).isEqualTo("Symbol");
        assertThat(actual.getMeta().getRefreshed()).isEqualTo("Last Refreshed");
        assertThat(actual.getMeta().getSize()).isEqualTo("Output Size");
        assertThat(actual.getMeta().getTimeZone()).isEqualTo("Time Zone");
    }

    @Test
    public void deserializeShouldSetTheDailyTimePeriod() throws IOException {
        StockTimeSeries actual = underTest.deserialize(parser, context);

        assertThat(actual.getTimePeriod()).isEqualTo(TimePeriods.DAILY);
    }

    @Test
    public void deserializeWhenInputIsADailyTimeSeries() throws IOException {
        List<ExchangeRate> expectedExchangeRates = asList(
            ExchangeRate.builder()
                .time(LocalDateTime.parse("2019-09-05 00:00:00", DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")))
                .volume(8425620)
                .price(Price.builder()
                    .open(139d)
                    .close(139.3932d)
                    .low(138.77d)
                    .high(140.3837d)
                    .build())
                .build(),
            ExchangeRate.builder()
                .time(LocalDateTime.parse("2019-09-04 00:00:00", DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")))
                .volume(15144888)
                .price(Price.builder()
                    .open(137.3d)
                    .close(137.63d)
                    .low(136.48d)
                    .high(137.69d)
                    .build())
                .build()
        );

        StockTimeSeries actual = underTest.deserialize(parser, context);

        assertThat(actual.getSeries()).isEqualTo(expectedExchangeRates);
    }

}
